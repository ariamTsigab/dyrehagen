
#ifndef Dyr_hpp
#define Dyr_hpp

#include <iostream>
#include <vector>
#include <string>
#include <memory>

using namespace std;

class Dyr {
    private:
        string name;
        string gender;
        string type;
        string weight;

    public:
        Dyr(string t_name, string t_type, string t_gender, string t_weight);
        Dyr(const Dyr& t_Dyr);
        Dyr();
        virtual ~Dyr();
        void setName(string t_name);
        string getName();
        void setGender(string t_gender);
        string getGender();
        void setType(string t_type);
        string getType();
        void setWeight(string t_weight);
        string getWeight();
        virtual string bevegelse() = 0;
};
#endif /* Dyr_hpp */
