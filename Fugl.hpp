#ifndef Fugl_hpp
#define Fugl_hpp

#include "Dyr.hpp"
#include <string>
using namespace std;

class Fugl : public Dyr {

    public:
        Fugl(string name, string gender, string type, string weight);
        Fugl();
        virtual ~Fugl();
        std::string bevegelse();
};
#endif /* Fugl_hpp */
