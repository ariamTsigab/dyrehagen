//
//  Dyrehagen.hpp
//  dyrehagen
//
//  Created by Tsigab Angosom Gebremedhin on 13/04/2021.
//

#ifndef Dyrehagen_hpp
#define Dyrehagen_hpp

#include <iostream>
#include <vector>
#include "Dyr.hpp"

using namespace std;

class Dyrehagen {
private:
    vector<Dyr*> dyreList;
public:
    Dyrehagen();
    Dyrehagen(vector<Dyr*> t_dyreList);
    Dyrehagen(const Dyrehagen& t_dyrehagen);
    
    void setDyrehagen(vector<Dyr*> t_dyreList);
    vector<Dyr*> getDyreList();
    
    void welcome();
    void addDyr(Dyr* new_dyr);
    void showDyr();
    void deleteDyr();

};
#endif /* Dyrehagen_hpp */
