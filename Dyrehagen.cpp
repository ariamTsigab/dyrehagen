#include <iostream>
#include <vector>
#include <string>
#include <memory>
#include "Dyrehagen.hpp"
#include "Dyr.hpp"


Dyrehagen::Dyrehagen(vector<Dyr*> t_dyreList) {
    this->dyreList = t_dyreList;
}

Dyrehagen:: Dyrehagen(const Dyrehagen& t_dyrehagen) : dyreList(t_dyrehagen.dyreList)
{}

Dyrehagen::Dyrehagen() {
    welcome();
}

void Dyrehagen::welcome() {
    std::cout << "\nVelkommen til Dyrehagen\n" << endl;
}

void Dyrehagen::setDyrehagen(vector<Dyr*> t_dyreList) {

}
vector<Dyr*> Dyrehagen::getDyreList() {
    return this->dyreList;
}

void Dyrehagen::addDyr(Dyr* new_dyr){
    dyreList.push_back(new_dyr);
}
void Dyrehagen::showDyr(){
    for(int i=0; i < dyreList.size(); i++) {
        cout<< dyreList[i]->getName()+":" << "\t\t" << dyreList[i]->getGender()+","<< "\t" <<dyreList[i]->getType()+","<< "\t" << dyreList[i]->getWeight()+","<< dyreList[i]->bevegelse()<< endl;
    }
   
}

void Dyrehagen::deleteDyr(){
    dyreList.clear();
}
