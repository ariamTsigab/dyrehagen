#ifndef Pattedyr_hpp
#define Pattedyr_hpp

#include "Dyr.hpp"
#include <string>
using namespace std;

class Pattedyr : public Dyr {
    public:
        Pattedyr(string name, string gender, string type, string weight);
        Pattedyr();
        virtual ~Pattedyr();
        string bevegelse();
};

#endif /* Pattedyr_hpp */
