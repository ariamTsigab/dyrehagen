
#include <iostream>
#include "Dyr.hpp"
#include "Dyrehagen.hpp"
#include "Fisk.hpp"
#include "Fugl.hpp"
#include "Pattedyr.hpp"

int main(int argc, const char * argv[]) {
    
    Dyrehagen* dyrehagen = new Dyrehagen;
    Dyr* tempA;
    
    dyrehagen->welcome();

    tempA = new Pattedyr("Henry", "Han", "Zebra" , "50kg");
    dyrehagen->addDyr(tempA);
    tempA = new Pattedyr("Anne", "Hun", "Zebra" , "45kg");
    dyrehagen->addDyr(tempA);
    tempA = new Pattedyr("Heidi", "Hun", "Zebra" ,"25kg");
    dyrehagen->addDyr(tempA);
    tempA = new Pattedyr("Ole", "Han", "Bjørn" ,  "100kg");
    dyrehagen->addDyr(tempA);
    tempA = new Pattedyr("Inger", "Hun", "Bjørn" ,  "80kg");
    dyrehagen->addDyr(tempA);
    tempA = new Pattedyr("Silje", "Hun","Bjørn" ,   "75kg");
    dyrehagen->addDyr(tempA);
    tempA = new Pattedyr("Peder",  "Han","Bjørn" ,   "100kg");
    dyrehagen->addDyr(tempA);
    tempA = new Pattedyr("Kari", "Hun", "Elg" , "90kg");
    dyrehagen->addDyr(tempA);
    tempA = new Pattedyr("Hans", "Han", "Rev" , "10kg");
    dyrehagen->addDyr(tempA);
    tempA = new Pattedyr("Marit", "Hun", "Rev" , "12kg");
    dyrehagen->addDyr(tempA);
    dyrehagen->addDyr(new Pattedyr("Lars", "Han", "Lama" , "40kg"));
    dyrehagen->addDyr(new Pattedyr("Ingrid", "Hun", "Lama" , "40kg"));
    dyrehagen->addDyr(new Pattedyr("Anders", "Han", "Lama" , "40kg"));
    dyrehagen->addDyr(new Pattedyr("Liv", "Hun", "Lama" , "40kg"));
    dyrehagen->addDyr(new Pattedyr("Niels", "Han", "Kenguru" , "50kg"));
    dyrehagen->addDyr(new Pattedyr("Maria ", "Han", "Kenguru" , "30kg"));
    dyrehagen->addDyr(new Pattedyr("Jens", "Han", "Kenguru", "1kg"));
    dyrehagen->addDyr(new Fisk("Johannes", "Han", "Torsk", "3kg"));
    dyrehagen->addDyr(new Fisk("Ida", "Hun", "Gullfisk", "100g"));
    dyrehagen->addDyr(new Fisk("Knud", "Han", "Guppy", "110g"));
    dyrehagen->addDyr(new Fisk("Anna", "Hun" , "Sverddrager", "80g"));
    dyrehagen->addDyr(new Fisk("Erich", "Han", "Kampfisk", "50g"));
    dyrehagen->addDyr(new Fisk("Hilde", "Hun", "Tetra", "40g"));
    tempA = new Fisk("Jacob", "Han", "Pansermalle", "50g");
    dyrehagen->addDyr(tempA);
    tempA = new Fisk("Nina", "Hun","Algeeter", "20g");
    dyrehagen->addDyr(tempA);
    tempA = new Fugl("John", "Han", "dvergspurv", "10g"); dyrehagen->addDyr(tempA);
    
    dyrehagen->addDyr(new Fugl("Marianne", "Hun", "dvergspurv", "10g"));
    dyrehagen->addDyr(new Fugl("Jon", "Han", "dvergspurv", "9g"));
    dyrehagen->addDyr(new Fugl("Elisabeth", "Hun", "lappspurv", "10g"));
    dyrehagen->addDyr(new Fugl("Christen", "Han", "lappspurv", "12g"));
    dyrehagen->addDyr(new Fugl("Berit", "Hun", "svartrødstjert", "10g"));
    dyrehagen->addDyr(new Fugl("Kristin", "Hun", "svartrødstjert", "10g"));
    dyrehagen->addDyr(new Fugl("Christopher", "Han", "sanglerke", "16g"));
    dyrehagen->addDyr(new Fugl("Astrid", "Hun", "sanglerke", "7g"));
    dyrehagen->addDyr(new Fugl("Rasmus", "Han", "slagugle", "100g"));
    dyrehagen->addDyr(new Fugl("Randi", "Hun", "slagugle", "10g"));
    std::sort(dyrehagen->getDyreList().begin(), dyrehagen->getDyreList().end(), [](Dyr* lhs, Dyr* rhs){
        return lhs->getType() < rhs->getType();
    });
    dyrehagen->showDyr();
    dyrehagen->deleteDyr();
  
}


