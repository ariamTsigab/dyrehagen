#include "Fugl.hpp"
#include <string>
using namespace std;


Fugl::Fugl(string name, string gender, string type, string weight): Dyr::Dyr(name, gender, type, weight)
{}

Fugl::Fugl() : Dyr() {
    this->setType("Fugl");
}
Fugl::~Fugl() {

}

string Fugl::bevegelse() {
    return "Fly";
}
