#include <iostream>
#include <vector>
#include <string>
#include "Dyr.hpp"

using namespace std;

Dyr::Dyr(string t_name,  string t_gender, string t_type, string t_weight): name(t_name), gender(t_gender), type(t_type), weight(t_weight) {

}
Dyr::Dyr(const Dyr& t_Dyr):
name(t_Dyr.name), gender(t_Dyr.gender), type(t_Dyr.type), weight(t_Dyr.weight) {

}

Dyr::Dyr() {
    this->name = "";
    this->type = "";
    this->gender = "";
    this->weight = "";
}

Dyr::~Dyr() {
    delete &this->name;
    delete &this->type;
    delete &this->gender;
    delete &this->weight;
}

void Dyr::setName(string t_name) {
    this->name = t_name;
}
string Dyr::getName() {
    return name;
}
void Dyr::setGender(string t_gender) {
    this->gender = t_gender;
}
string Dyr::getGender() {
    return gender;
}
void Dyr::setType(string t_type) {
    this->type = t_type;
}
string Dyr::getType() {
    return this->type;
}
void Dyr::setWeight(string t_weight) {
    this->weight = t_weight;
}

string Dyr::getWeight() {
    return this->weight;
}

