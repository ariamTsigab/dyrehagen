#ifndef Fisk_hpp
#define Fisk_hpp

#include "Dyr.hpp"
#include <string>
using namespace std;

class Fisk : public Dyr {
    public:
        Fisk(string name, string gender, string type, string weight);
        Fisk(const Fisk &t_fisks);
        virtual ~Fisk();
        string bevegelse();
};
#endif /* Fisk_hpp */
